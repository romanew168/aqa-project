package com.mikhailovApi.app.endpoints;

import com.mikhailovApi.app.model.PojoCredit;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.json.JSONException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import java.util.Properties;
import java.util.Random;

@RestController
@Api(tags = "Сервис обработки заявок на кредит")
public class ApiCredit {
    private String basePath = "/main/resources/jsonRequest/";


    private static Session session;
    private static Properties props = new Properties();

    static {
        try {
            props.setProperty("hibernate.connection.url",
                    "jdbc:mysql://localhost:3306/credit");
            props.setProperty("hibernate.connection.user",
                    "user");
            props.setProperty("hibernate.connection.password",
                    "user");
            props.setProperty("dialect",
                    "org.hibernate.dialect.MySQL8Dialect");
            // Table auto creation
            props.setProperty("hibernate.hbm2ddl.auto", "update");
            props.setProperty("show_sql", "true");
            SessionFactory sessionFactory = new Configuration()
                    .addAnnotatedClass(com.mikhailovApi.app.model.PojoCredit.class)
                    .addProperties(props)
                    .buildSessionFactory();
            session = sessionFactory.openSession();
        } catch (Exception fff) {
            throw fff;
        }
    }


    @Operation(summary = "Получение заявки на кредит")
    @GetMapping(path = "/credit/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getCredit(@PathVariable Integer id) throws JSONException {
        return (PojoCredit) session.get(com.mikhailovApi.app.model.PojoCredit.class, id);

    }

    @Operation(summary = "Создание заявки на кредит")
    @PostMapping(path = "/credit", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object postCredit(@RequestBody PojoCredit pojo) throws JSONException {
        try {
            String result = resultCredit(pojo.getName(), pojo.getAge(), pojo.getSumm());
            pojo.setResult(result);
            Transaction transaction = session.beginTransaction();
            session.save(pojo);
            transaction.commit();
            session.clear();
            return pojo;

        } catch (Exception newException) {
            return "{\"error\": \"Запрос должен быть в формате JSON с тегами name, age, summ\"}";
        }

    }

    @Operation(summary = "Удаление заявки на кредит")
    @DeleteMapping(path = "/credit/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String deleteCredit(@PathVariable Integer id) throws JSONException {
        try {
            PojoCredit pojo = (PojoCredit) getCredit(id);
            Transaction transaction = session.beginTransaction();
            session.delete(pojo);
            transaction.commit();

            return "{\"success\":\"true\"}";
        } catch (Exception newException) {
            return "{\"success\":\"false\"}";
        }
    }

    @Operation(summary = "Изменение заявки на кредит")
    @PutMapping(path = "/credit/{id}", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object updateCredit(@RequestBody PojoCredit pojoupdate, @PathVariable Integer id) throws JSONException {
        session.clear();
        pojoupdate.setId(id);
        String result = resultCredit(pojoupdate.getName(), pojoupdate.getAge(), pojoupdate.getSumm());
        pojoupdate.setResult(result);
        Transaction transaction = session.beginTransaction();
        session.update(pojoupdate);
        transaction.commit();

        return pojoupdate;


    }

    @Operation(summary = "Удаление случайной заявки")
    @DeleteMapping(path = "/credit/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String deleteRandomCredit() throws JSONException {
        List<PojoCredit> pojolist = session.createQuery("From PojoCredit").list();
        Random random = new Random();
        int rnd = random.nextInt(pojolist.size());
        System.out.println("количество строк в БД "+ pojolist.size());
        int id = pojolist.get(rnd).getId();
        System.out.println("Удаляем id "+ id);
        return deleteCredit(id);
    }


    private String resultCredit(String name, int age, int summ) {

        String result = (age >= 18) && (!name.equals("Bob")) && (summ < age * 100) ? "Кредит одобрен"
                : "Отказано в кредите";
        return result;
    }


}
