package com.mikhailovApi.app.endpoints;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiCalculateController {
    @GetMapping(path = "/multi/{num}", produces = MediaType.APPLICATION_JSON_VALUE)

    @ResponseBody
    public String multi(@PathVariable String num) throws JSONException {

        JSONObject json = new JSONObject();
        int m1 = Integer.parseInt(num);
        int m = m1;
        for (int i = 1; i <= 4; i++) {
            json.put(String.valueOf(i), m);
            m *= m1;
        }
        return json.toString();
    }


    @GetMapping(path = "/multi", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String multi(){
        String s = "через слэш в адресной строке укажите число которое нужно возвести в степень";
        return s;
    }


}
