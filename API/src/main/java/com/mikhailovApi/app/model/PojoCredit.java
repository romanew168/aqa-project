package com.mikhailovApi.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Objects;


@Entity
@Table(name = "dbcredit", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id")
})
public class PojoCredit {
    private Integer id;
    private String name;
    private Integer age;
    private Integer summ;
    private String result;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false,unique = true)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "age", nullable = false)
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Column(name = "summ", nullable = false)
    public Integer getSumm() {
        return summ;
    }

    public void setSumm(Integer summ) {
        this.summ = summ;
    }

    @Column(name = "result", nullable = false)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PojoCredit that = (PojoCredit) o;
        return id.equals(that.id) && name.equals(that.name) && age.equals(that.age) && summ.equals(that.summ) && result.equals(that.result);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, age, summ, result);
    }


}
