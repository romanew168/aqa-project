package com.mikhailovApi.app;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class AppApplicationTests {

    @LocalServerPort
    public Integer port;
    public static RequestSpecification request;

    @BeforeEach
    public void createRestRequest() {
        request = RestAssured.given();
        RestAssured.baseURI = "http://localhost:" + port;

    }


}
