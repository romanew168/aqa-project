package com.mikhailovApi.app.endpoints;

import com.mikhailovApi.app.AppApplicationTests;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class ApiCalculateTest extends AppApplicationTests {


    @ParameterizedTest
    @CsvSource({"5,25,625", "2,4, 16"})
    public void multi(String n1, String n2, String n3) {
        Response response = request.get("/multi/" + n1);
        assertEquals(n2, response.jsonPath().getString("2"));
        assertEquals(n3, response.jsonPath().getString("4"));
    }

    @Test
    public void multi2() {
        Response response = request.get("/multi/");
        assertEquals("через слэш в адресной строке укажите число которое нужно возвести в степень", response.getBody().print());

    }
}