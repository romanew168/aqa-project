package com.mikhailovApi.app.endpoints;

import com.mikhailovApi.app.AppApplicationTests;
import io.restassured.response.Response;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ApiCreditTest extends AppApplicationTests {

    public static List<Integer> ids = new ArrayList<>();

    @ParameterizedTest
    @Order(1)
    @CsvSource({"Роман, 31, 3000, Кредит одобрен",
            "Bob, 31, 3000, Отказано в кредите",
            "Александр, 25, 5000, Отказано в кредите",
            "Мария, 16, 1000,Отказано в кредите"})
    void postCredit(String name, String age, String summ, String result) {
        Response response = request
                .contentType("application/json")
                .body("{\n\"name\":\"" + name + "\",\n \"age\":" + age + ",\n \"summ\":" + summ + "\n}")
                .post("/credit");
        response.print();
        assertEquals(name, response.jsonPath().getString("name"));
        assertEquals(age, response.jsonPath().getString("age"));
        assertEquals(summ, response.jsonPath().getString("summ"));
        assertEquals(result, response.jsonPath().getString("result"));
        ids.add(response.jsonPath().getInt("id"));
    }

    @ParameterizedTest
    @Order(2)
    @CsvSource({"Роман, 31, 3000, Кредит одобрен,0",
            "Bob, 31, 3000, Отказано в кредите, 1",
            "Александр, 25, 5000, Отказано в кредите,2",
            "Мария, 16, 1000,Отказано в кредите,3"})
    void getCreditSuccess(String name, String age, String summ, String result, int num) {
        int id = ids.get(num);
        Response response = request.get("/credit/" + id);
        response.print();
        assertEquals(name, response.jsonPath().getString("name"));
        assertEquals(age, response.jsonPath().getString("age"));
        assertEquals(summ, response.jsonPath().getString("summ"));
        assertEquals(result, response.jsonPath().getString("result"));
    }

    @ParameterizedTest
    @Order(3)
    @CsvSource({"Ксения, 18, 3000, Отказано в кредите,0",
            "Том, 41, 3000, Кредит одобрен, 1",
            "Евгения, 35, 10000, Отказано в кредите,2",
            "Мария Привет, 15, 1000,Отказано в кредите,3"})
    void updateCreditTest(String name, String age, String summ, String result, int num) {
        Integer id = ids.get(num);
        System.out.println(id + " id");
        Response response = request
                .contentType("application/json")
                .body("{\n\"name\":\"" + name + "\",\n \"age\":" + age + ",\n \"summ\":" + summ + "\n}")
                .put("/credit/" + id);
        response.print();
        assertEquals(name, response.jsonPath().getString("name"));
        assertEquals(age, response.jsonPath().getString("age"));
        assertEquals(summ, response.jsonPath().getString("summ"));
        assertEquals(result, response.jsonPath().getString("result"));
    }


    @Test
    @Order(4)
    void deleteAllCredit() {
        for (int num = 0; num < ids.size(); num++) {
            int id = ids.get(num);
            System.out.println(id);
            Response response = request.delete("/credit/" + id);
            response.print();
            assertEquals("true", response.jsonPath().getString("success"));
        }

    }


    @Test
    @Order(5)
    void getCreditNotFound() {
        for (int num = 0; num < ids.size(); num++) {
            int id = ids.get(num);
            Response response = request.get("/credit/" + id);
            response.print();
            System.out.println(id + " id не найден");
            assertEquals("", response.print());
        }
    }


    @Test
    @Order(6)
    void deleteCreditFalse() {
        for (int num = 0; num < ids.size(); num++) {
            int id = ids.get(num);
            System.out.println(id);
            Response response = request.delete("/credit/" + id);
            response.print();
            assertEquals("false", response.jsonPath().getString("success"));
        }
    }

    @Test
    @Order(7)
    void postCreditError() {
        Response response = request
                .contentType("application/json")
                .body("{\n\"name\":\"Роман\", \"summ\": 30\n}")
                .post("/credit");
        response.print();
        assertEquals("Запрос должен быть в формате JSON с тегами name, age, summ", response.jsonPath().getString("error"));
    }

    @Test
    @Order(0)
    public void deleteRandomCredit() {
        Response response = request.delete("/credit/");
        response.print();
        assertEquals("true", response.jsonPath().getString("success"));

    }

    // Валидация методов
    @Test
    @Order(9)
    public void swaggerTest() throws IOException {
        Path path = Path.of("/main/resources/jsonSchema");
        String swaggerSchema = Files.readString(path);
        request.get("/v2/api-docs").then().assertThat().
                body(matchesJsonSchema(swaggerSchema));
    }


}
