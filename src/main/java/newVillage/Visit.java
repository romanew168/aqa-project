package main.java.newVillage;


import java.io.IOException;

import static main.java.newVillage.Events.*;

public class Visit {


    public static void visit() throws IOException {
        int i = 0, j = 0;
        String date = dateFormat.format(dataOfEvent.getTime());

        if (Tomsk.inhabitants.size() > 1) {
            i = random.nextInt(Tomsk.inhabitants.size());
            String people1 = Tomsk.inhabitants.get(i);
            String people2 = "";

            do {
                j = random.nextInt(Tomsk.inhabitants.size());
                people2 = Tomsk.inhabitants.get(j);
            }
            while (people1.equals(people2));

            if ((date.matches("13...........пятница")
                    || date.contains("31 окт."))
                    && !people1.contains("Witch") && people2.contains("Witch")) {
                try {
                    MurderException();
                } catch (Exception MURDEREXEPTION) {
                    Tomsk.inhabitants.remove(i);
                    event = date + " MURDEREXCEPTION " + " " + people1 + " " + event + " " + people2 + ". В итоге встречи сожрали " + people1;
                    System.out.println(event);
                    ToFile.toFile(event);
                }

            } else if ((date.matches("13...........пятница")
                    || date.contains("31 окт."))
                    && !people2.contains("Witch") && people1.contains("Witch")) {
                try {
                    MurderException();

                } catch (Exception MURDEREXEPTION) {
                    Tomsk.inhabitants.remove(j);
                    event = date + " MURDEREXCEPTION " + " " + people1 + " " + event + " " + people2 + ". В итоге встречи сожрали " + people2;
                    System.out.println(event);
                    ToFile.toFile(event);

                }
            } else if (people1.contains("Vampire")) {
                vampire(people1, people2, i, j);

            } else if (people2.contains("Vampire")) {
                vampire(people2, people1, j, i);

            } else if ((people1.contains("Werewolf") && Moon.fullmoons.contains(date))) {
                Tomsk.inhabitants.remove(i);
                event = date + " MURDEREXCEPTION " + " " + people1 + " " + event + " " + people2 + " в полнолуние. в итоге исчез " + people2;
                System.out.println(event);
                ToFile.toFile(event);


            } else if ((people2.contains("Werewolf") && Moon.fullmoons.contains(date))) {
                Tomsk.inhabitants.remove(i);
                event = date + " MURDEREXCEPTION " + " " + people1 + " " + event + " " + people2 + " в полнолуние. в итоге исчез " + people2;
                System.out.println(event);
                ToFile.toFile(event);


            } else if (people1.contains("Witch") && people2.contains("Werewolf") && Moon.fullmoons.contains(date) && date.contains("31 окт.")) {

                event = date + " Ведьма " + people1 + " " + event + " " + people2 + " в полнолуние 31 октября. и они обсуждают свой коварный план";
                System.out.println(event);
                ToFile.toFile(event);

            } else if (people2.contains("Witch") && people1.contains("Werewolf") && Moon.fullmoons.contains(date) && date.contains("31 окт.")) {

                event = date + " Ведьма " + people1 + " " + event + " " + people2 + " в полнолуние 31 октября. и они обсуждают свой коварный план";
                System.out.println(event);
                ToFile.toFile(event);

            } else {
                event = date + " " + people1 + " " + event + " " + people2 + " И просто пьют чай и болтают про ведьм";
                System.out.println(event);
                ToFile.toFile(event);

            }

            // System.out.println(date + " " + people1 + " " + event + " " + people2);
        } else {
            event = date + " Ничего не произошло. Жители которые сейчас в городе: " + Tomsk.inhabitants + "//" + Tomsk.inhabitants.size();
            System.out.println(event);
            ToFile.toFile(event);


        }
    }

    public static void vampire(String people1, String people2, int i, int j) throws IOException {
        String date = dateFormat.format(dataOfEvent.getTime());
        int v = random.nextInt(20);
        int w = random.nextInt(100);

        if ((people2.contains("Peasant") || people2.contains("Witch")) && (v == 2)) {
            Tomsk.inhabitants.set(j, people2.substring(0, people2.indexOf("(")) + "(Vampire)");
            String people3 = Tomsk.inhabitants.get(j);

            event = date + " " + people1 + " " + event + " " + people2 + ". В итоге встречи стал вампиром " + people3;
            System.out.println(event);
            ToFile.toFile(event);


        } else if (v == 1) {
            try {
                MurderException();

            } catch (Exception MURDEREXEPTION) {
                Tomsk.inhabitants.remove(j);
                event = date + " MURDEREXCEPTION " + " " + people1 + " " + event + " " + people2 + ". В итоге встречи Убит " + people2;
                System.out.println(event);
                ToFile.toFile(event);

            }
        } else if (w == 1) {
            Tomsk.inhabitants.remove(i);
            event = date + " MURDEREXCEPTION " + " " + people1 + " " + event + " " + people2 + ". В итоге исчез из города вампир " + people1;
            System.out.println(event);
            ToFile.toFile(event);


        } else {
            event = date + " " + people1 + " " + event + " " + people2 + " И просто пьют кофеёк и болтают про вампиров";
            System.out.println(event);
            ToFile.toFile(event);


        }

    }

    public static void MurderException() {
        int a = 0, b = 0;
        a /= b;
    }

}
