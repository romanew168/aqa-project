package main.java.newVillage;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Scanner;

public class ToFile {

    private static Date date = new Date();
    private static long history = date.getTime();
    private static FileWriter file, copyfile;

    static {
        try {
            file = new FileWriter("/main/java/newVillage/files/history_" + history + ".txt");
            copyfile = new FileWriter("/main/java/newVillage/files/history_copy_" + history + ".txt");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Ошибка создания файла");
        }
    }

    public static void toFile(String eventToFile) throws IOException {
        file.write(eventToFile + "\n");
    }

    public static void closeFile() throws IOException {
        file.close();
    }

    public static void fileReader() throws IOException {
        FileReader fr = new FileReader("/main/java/newVillage/files/history_" + history + ".txt");
        Scanner scan = new Scanner(fr);
        int i = 1;
        while (scan.hasNext()) {
            String str = scan.nextLine();
            System.out.println(str);
            copyfile.write(str + "\n");
            i++;
        }
        copyfile.close();
    }

}
