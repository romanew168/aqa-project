package main.java.newVillage;

import java.util.Random;

public class Peoples {
    private static String[] names = {"Stacey", "Dianne","Robert","Bonnie", "Chandler", "Ross", "Joey", "Monica", "Phoebe", "Rachel", "Jack", "Hugo", "Shannon", "James", "Kate", "Michael", "Jim", "Fred"};
    private static String[] surnames = {"Foster", "Carlson", "Green", "Geller", "Buffay", "Tribbiani", "Bing", "Shephard", "Ford", "Austen", "Dawson", "Duck", "Swan", "Cooper"};

    static String newpeople() {
        Random random = new Random();
        String nameRandom = names[random.nextInt(names.length)];
        String surnameRandom = surnames[random.nextInt(surnames.length)];
        Village.peoples.add(nameRandom + " "+ surnameRandom);
        String people = nameRandom + " " + surnameRandom;
        return people;

    }


}