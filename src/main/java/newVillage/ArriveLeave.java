package main.java.newVillage;

import java.io.IOException;

import static main.java.newVillage.Events.*;

public class ArriveLeave {

    public static void arrive() throws IOException {

        if (Tomsk.inhabitantsOff.size() != 0) {
            int i = random.nextInt(Tomsk.inhabitantsOff.size());
            String people = Tomsk.inhabitantsOff.get(i);
            Tomsk.inhabitants.add(Tomsk.inhabitantsOff.get(i));
            Tomsk.inhabitantsOff.remove(i);

            event = dateFormat.format(dataOfEvent.getTime()) + " " + people + " " + event;
            System.out.println(event);
            ToFile.toFile(event);

            //      System.out.println(Tomsk.inhabitantsOff + "//" + Tomsk.inhabitantsOff.size());  //массив людей которые уехали
        } else {
            Events.nothing();

        }
    }


    public static void leave() throws IOException {
        if (Tomsk.inhabitants.size() != 0) {
            int i = random.nextInt(Tomsk.inhabitants.size());
            String people = Tomsk.inhabitants.get(i);
            Tomsk.inhabitantsOff.add(Tomsk.inhabitants.get(i));
            Tomsk.inhabitants.remove(i);
            event = dateFormat.format(dataOfEvent.getTime()) + " " + people + " " + event;
            System.out.println(event);
            ToFile.toFile(event);


        } else {
            Events.nothing();

        }
    }
}
