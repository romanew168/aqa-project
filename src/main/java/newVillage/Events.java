package main.java.newVillage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class Events {


    public static String typeOfEvent, event;
    public static Calendar dataOfEvent, dateOfEndHistory;
    static String[] events = {"allSleep", "visit", "arrives", "leave"};
    public static ArrayList<String> dateOfEvents = new ArrayList<String>();
    static SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy EEEE");

    static Random random = new Random();
    static Village Tomsk = new Village();


    public String getTypeOfEvent() {

        int i = random.nextInt(events.length);
        typeOfEvent = events[i];
        // System.out.println(typeOfEvent); //Событие
        return typeOfEvent;
    }

    public void newEvent() throws IOException {

        dataOfEvent = new GregorianCalendar(1650, 0, 0);
        dateOfEndHistory = new GregorianCalendar(1750, 11, 30);
        Tomsk.createvillage();
        Moon.fullMoon();


        //System.out.println(Moon.fullmoons); //в эти дни новолуние
        System.out.println("Сейчас жителей в городе: " + Tomsk.inhabitants.size());
        ToFile.toFile("Сейчас жителей в городе: " + Tomsk.inhabitants.size());
        while (dateOfEndHistory.getTime().getTime() >= dataOfEvent.getTime().getTime()) {

            dataOfEvent.add(Calendar.DAY_OF_MONTH, 1);
            dateOfEvents.add(dateFormat.format(dataOfEvent.getTime()));
            getTypeOfEvent();
            eventLogic();

        }
        ToFile.toFile("Итого сейчас жителей в городе: " + Tomsk.inhabitants.size());
        ToFile.toFile("Уехало из города: " + Tomsk.inhabitantsOff.size());


        System.out.println("Итого сейчас жителей в городе: " + Tomsk.inhabitants.size());
        System.out.println("Уехало из города: " + Tomsk.inhabitantsOff.size());

        ToFile.closeFile();
    }

    public void eventLogic() throws IOException {

        switch (typeOfEvent) {
            case "allSleep" -> {
                nothing();
                break;
            }
            case "visit" -> {
                event = "Идет в гости к ";
                Visit.visit();
                break;

            }
            case "arrives" -> {
                event = "Приехал в город";
                ArriveLeave.arrive();
                break;
            }
            case "leave" -> {
                event = "Уехал из города";
                ArriveLeave.leave();
                break;

            }
        }


    }

    public static void nothing() throws IOException {
        event = dateFormat.format(dataOfEvent.getTime()) + " " + "Совсем ничего не произошло, все гуляли";

        //  ToFile.toFile(event);
        // System.out.println(event); // Тут можно закоментировать чтобы не выводилось!!
    }


}