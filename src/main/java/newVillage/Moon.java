package main.java.newVillage;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static main.java.newVillage.Events.dateFormat;

public class Moon {
    public static ArrayList<String> fullmoons = new ArrayList<String>();
    static Calendar fullmoon, dateOfEndHistory;

    public static void fullMoon() {

        fullmoon = new GregorianCalendar(1650, 0, 17);
        dateOfEndHistory = new GregorianCalendar(1750, 11, 7);
        while (dateOfEndHistory.getTime().getTime() > fullmoon.getTime().getTime()) {
            fullmoon.add(Calendar.DAY_OF_MONTH, 28);

            fullmoons.add(dateFormat.format(fullmoon.getTime()));
            //System.out.println(fullmoons); //в эти дни новолуние
        }

    }

}
