package main.java.app;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class HomeWork4 {
    public static void main(String[] args) {

        System.out.println("Выберите сложность игры 1 - Легко, 2 - Средне, 3 - Сложно");
        Scanner input = new Scanner(System.in);
        int level = input.nextInt();
        int number = 5;
        System.out.println("Ниже дана последовательность, угадайте следующий номер");
        if (level == 1) {
            for (int i = 0; i < 3; i++) {
                number -= 1;
                System.out.print(number);
                System.out.print("  ");
                number *= 2;
                System.out.print(number);
                System.out.print("  ");

            }
            number -= 1;
        } else if (level == 2) {
            for (int i = 0; i < 3; i++) {
                number -= 2;
                System.out.print(number);
                System.out.print("  ");
                number *= 2;
                System.out.print(number);
                System.out.print("  ");
                number += 4;
                System.out.print(number);
                System.out.print("  ");

            }
            number -= 2;
        } else if (level == 3) {
            Random random = new Random();
            int r = random.nextInt(5);
            for (int i = 0; i < 3; i++) {
                number -= r;
                System.out.print(number);
                System.out.print("  ");
                number *= 2;
                System.out.print(number);
                System.out.print("  ");
                number += (r + 5);
                System.out.print(number);
                System.out.print("  ");
            }
            number -= r;
        } else {
            System.out.println("Вы ввели не верный уровень сложности, введите 0 чтобы выйти");

        }
        for (int i = 2; i >= 0; i--) {
            int answer = input.nextInt();
            if (answer == number) {
                System.out.println("Правильно");

                i = 0;
            } else {
                System.out.println("Не верно! У вас осталось " + i + " попытки");
            }
        }
        System.out.println("Правильный ответ: " + number);
    }

}
