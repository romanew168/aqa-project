package main.java.app;

import java.util.Scanner;

// Домашнее задание калькулятор by Михайлов Роман
public class HomeWork5 {
    public static void main(String[] args) {
        String primer = "";
        Scanner input = new Scanner(System.in);
        int validate = 0;
        while (validate == 0) {
            System.out.println("Напишите что вы хотите посчитать, например 1+2 ");
            primer = input.nextLine();

            if (primer.matches("[0-9][0-9]*[ ]*[*+/-][ ]*[0-9][0-9]*")) {
                System.out.println(primer);
                validate = 1;
            } else {
                System.out.println("Вы не правильно ввели пример, попробуйте еще раз");
            }
        }
        String[] numbers = primer.split("[ ]*[*+/-][ ]*");
        double num1 = Double.parseDouble(numbers[0]);
        double num2 = Double.parseDouble(numbers[1]);
        double calc = 0;

        if (primer.contains("+")){
            calc = num1+num2;
        } else if (primer.contains("-")){
            calc = num1-num2;
        } else if (primer.contains("*")){
            calc = num1*num2;
        } else if (primer.contains("/")){
            calc = num1/num2;
        }
        System.out.println(primer + " = "+ calc);

    }

}
