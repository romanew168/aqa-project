package main.java.Encoder;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputData {
    public static int action;
    public static String strString, key;


    public static void inputData() {

        System.out.println("Выберите действие:");
        System.out.println("1 - Зашифровать текст");
        System.out.println("2 - Расшифровать текст");

        Scanner input = new Scanner(System.in);
        try {
            action = input.nextInt();
            switch (action) {
                case 1: {
                    getToEncode();
                    getKey();
                    break;
                }
                case 2: {
                    getToDecode();
                    getKey();
                    break;
                }
                default: {
                    System.out.println("Неправильно введены данные, чтобы повторить ввод введите любое число");
                    System.out.println("Повторите ввод, или для выхода нажмите 0");
                    action = input.nextInt();
                    if (action == 0) {
                        System.exit(0);
                    } else {
                        inputData();
                    }
                }
            }
        } catch (InputMismatchException action) {
            System.out.println("Введите число, а не строку");
            inputData();
        }


    }

    private static void getToEncode() {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите строку для шифрования (для досрочного выхода из программы введите exit):");
        strString = input.nextLine();
        if (strString.length() > 0 && !(strString.equals("exit"))) {

            System.out.println("Зашифруем эту строку:  " + strString);

        } else if (strString.equals("exit")) {
            System.exit(0);

        } else {
            System.out.println("Вы ввели пустую строку, повторите ввод");
            System.out.println("для выхода наберите exit");
            getToEncode();

        }

    }

    private static void getToDecode() {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите строку для Дешифрования (для досрочного выхода из программы введите exit):");
        strString = input.nextLine();
        if (strString.length() > 0) {

            System.out.println("Дешифруем эту строку:  - " + strString);

        } else if (strString.equals("exit")) {
            System.exit(0);

        } else {
            System.out.println("Вы ввели пустую строку, повторите ввод");
            System.out.println("для выхода наберите exit");
            getToEncode();

        }

    }
    private static void getKey() {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите ключ шифрования/дешифрования (для досрочного выхода из программы введите exit):");
        key = input.nextLine();
        if (key.length() > 0 && !(key.equals("exit"))) {

            System.out.println("Ключ - " + key);

        } else if (key.equals("exit")) {
            System.exit(0);

        } else {
            System.out.println("Вы ввели пустую строку, повторите ввод");
            System.out.println("для выхода наберите exit");
            getKey();

        }

    }

}
