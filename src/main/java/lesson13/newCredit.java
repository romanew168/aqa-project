package main.java.lesson13;

import org.json.JSONArray;
import org.json.JSONObject;

public class newCredit {


    static JSONObject clients = new JSONObject();


    public static void main(String[] args) {
        createJSON();
        analizeJSON();
    }

    static void createJSON() {
        JSONArray peoples = new JSONArray();
        String[] name = {"Роман", "Ксения", "Саша", "Максим", "Кристина", "Екатерина", "Николай", "Юлия", "Маша", "Иван"};
        String[] age = {"31", "30", "40", "5", "25", "20", "40", "27", "35", "32"};
        String[] summ = {"3000", "3000", "4000", "1500", "2000", "3000", "4400", "2700", "3400", "3200"};
        for (int i = 0; i < name.length; i++) {
            JSONObject people = new JSONObject();
            people.put("name", name[i]);
            people.put("age", age[i]);
            people.put("summ", summ[i]);
            peoples.put(people);
        }
        clients.put("clients", peoples);
        System.out.println("Список клиентов = " + clients);
        System.out.println("===================");
    }

    static void analizeJSON() {

        JSONArray creditPeople = clients.getJSONArray("clients");
        JSONArray creditCliens = new JSONArray();

        for (int i = 0; i < creditPeople.length(); i++) {
            JSONObject people = creditPeople.getJSONObject(i);
            String name = people.getString("name");
            int age = people.getInt("age");
            int summ = people.getInt("summ");
            String credit = getCredit(name, age, summ);
            JSONObject creditclient = new JSONObject();
            creditclient.put("name", name);
            creditclient.put("age", age);
            creditclient.put("summ", summ);
            creditclient.put("credit", credit);
            creditCliens.put(creditclient);
            System.out.println(creditclient);
        }
        clients.put("clients", creditCliens);
        System.out.println("===================");
        System.out.println("Обработка заявок на кредит = " + clients);

    }

    public static String getCredit(String name, int age, int summ) {

        String result = (age >= 18) && (!name.equals("Bob")) && (summ < age * 100) ? "Кредит одобрен"
                : "отказано в кредите";
        return result;
    }

}

