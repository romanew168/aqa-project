package main.java.exam1;

import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;


public abstract class TestHelper {

    private static Random random = new Random();

    public static void main(String[] arg) throws IOException, ParseException {

        convert("54234.5345345б534.43434");


    }


    public static int randomYear() {
        int year = random.nextInt(2021 - 1970) + 1970;
        System.out.println(year);
        return year;
    }

    public static long longNum() {
        long num = random.nextLong();
        System.out.println(num);
        return num;
    }

    public static String newString() {
        String str1 = "abcdefghijklmnopqrstuvwxyz";
        String str2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String words = " ";
        for (int i = 0; i < 3; i++) {
            words += str2.charAt(random.nextInt(str2.length()));
            for (int j = 0; j < random.nextInt(10) + 2; j++) {
                words += str1.charAt(random.nextInt(str1.length()));
            }
            words += " ";
        }
        System.out.println(words);
        return words;
    }

    public static HashMap newMap(String path) throws IOException {
        HashMap<Integer, String> passportsAndNames = new HashMap<>();
        String[] strFile = fileReader(path).split("\n");
        for (String map : strFile) {
            String[] value = map.split("::");
            passportsAndNames.put(Integer.parseInt(value[0]), value[1]);
        }
        System.out.println(passportsAndNames);
        return passportsAndNames;
    }

    private static String fileReader(String path) throws IOException {
        FileReader fr = new FileReader(path);
        Scanner scan = new Scanner(fr);
        String str = "";
        int i = 0;
        while (scan.hasNext()) {
            str += scan.nextLine() + "\n";
            i++;
        }
        System.out.println("Cодержимое файла: ");
        System.out.println(str);
        return str;
    }


    public static String newFormat(Date date, String newformat) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(newformat);
        String dformatted = dateFormat.format(date);
        System.out.println("Изначальный формат даты: " + date);
        System.out.println("Новый формат формат даты: " + dformatted);
        System.out.println(dformatted);
        return dformatted;
    }

    public static String newFormat2(String date, String oldformat, String newformat) throws ParseException {
        SimpleDateFormat dateFormatNew = new SimpleDateFormat(newformat);
        SimpleDateFormat dateFormatOld = new SimpleDateFormat(oldformat);
        Date olddate = dateFormatOld.parse(date);
        String dformatted = dateFormatNew.format(olddate);
        System.out.println("Изначальный формат даты: " + date);
        System.out.println("Новый формат формат даты: " + dformatted);
        return dformatted;

    }

    public static Double convert(String str) {
        try {
            Double d = Double.parseDouble(str);
            System.out.println(d);
            return d;
        } catch (Exception Exception) {
            Double d = Double.POSITIVE_INFINITY;
            System.out.println(d);
            return d;
        }
    }


}