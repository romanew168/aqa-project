package main.java.Calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

// Домашнее задание калькулятор by Михайлов Роман

public class Calculator {
    private static String primer = "";
    private static int validate = 0;

    private static ArrayList<String> numbers = new ArrayList<>();
    private static ArrayList<Integer> ratings = new ArrayList<>();
    private static ArrayList<Character> actions = new ArrayList<>();


    public static void main(String[] args) {
        inputCalc();
        Numbers();
        ratingsNOperations();
        calc();
        System.out.println("Ответ: " + numbers.get(0));
    }

    private static void inputCalc() {

        Scanner input = new Scanner(System.in);
        while (validate == 0) {
            System.out.println("Напишите что вы хотите посчитать");
            primer = input.nextLine().replaceAll(" ", "");
            validate();

        }
    }

    private static void validate() {
        if (primer.matches("[-0-9(][0-9(.*+/-]*[0-9().*+/-]*[0-9).*+/-]*[0-9)]")) {
            validate = 1;
            System.out.println("Пример = " + primer);

        } else if (primer.equals("exit")) {

            System.exit(0);
        } else {

            System.out.println("Вы не правильно ввели пример, попробуйте еще раз или введите exit для выхода");
        }

    }

    private static void Numbers() {
        String[] rawNumbers = primer.split("[()*/+-]");
        numbers = new ArrayList<>(Arrays.asList(rawNumbers));
        numbers.removeIf(s -> s.length() == 0);
        Character isminus = primer.charAt(0);

        if (isminus.equals('-')) {   //если первый символ в строке с примером минус, то добавляем в начало массива цифр 0

            numbers.add(0, "0");
        }

    }

    private static void ratingsNOperations() {
        int rating = 0;
        for (int i = 0; i < primer.length(); i++) {
            if (primer.charAt(i) == '(') {
                rating += 2;
            }
            if (primer.charAt(i) == ')') {
                rating -= 2;
            }
            if ((primer.charAt(i) == '*') || (primer.charAt(i) == '/')) {
                actions.add(primer.charAt(i));
                ratings.add(rating + 1);
            }
            if ((primer.charAt(i) == '-') || (primer.charAt(i) == '+')) {
                actions.add(primer.charAt(i));
                ratings.add(rating);
            }
        }
    }

    private static void calc() {
        while (ratings.size() > 0) {
            int max = Collections.max(ratings);
            int index = ratings.indexOf(max);
            String result = calculate(actions.get(index), numbers.get(index), numbers.get(index + 1));
            ratings.remove(index);
            actions.remove(index);
            numbers.remove(index);
            numbers.set(index, result);
        }
    }

    private static String calculate(Character action, String num1, String num2) {
        Double result = 0.0;
        double one = Double.parseDouble(num1);
        double two = Double.parseDouble(num2);
        switch (action) {
            case '/':
                result = one / two;
                break;
            case '+':
                result = one + two;
                break;
            case '*':
                result = one * two;
                break;
            case '-':
                result = one - two;
                break;
        }
        return result.toString();
    }


}



