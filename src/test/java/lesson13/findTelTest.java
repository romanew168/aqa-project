package test.java.lesson13;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class findTelTest {

    String s = "14234238123488845234443812348888232895648888234423523";


    @Test
    void telNumbers() {
        ArrayList<String> test = findTel.telNumbers();
        String[] addnum = {"123", "345", "444", "564", "643", "472", "675", "553", "889", "555",
                "1423", "3456", "4144", "9564", "4643", "2345", "2355", "7896", "1255", "0098"};

        for (String num : addnum) {
            String telnum = "8";
            telnum = telnum + num;
            assert (test.contains(telnum));
        }
    }


    @Test
    void findNumber() {

        ArrayList<String> telFind = findTel.findNumber("81234", s);
        System.out.println(telFind);
        assertEquals("81234888452", telFind.get(0));
        assertEquals("81234888823", telFind.get(1));
    }

    @Test
    void changeNumber() {

        ArrayList<String> telFind = findTel.changeNumber(s);
        assertEquals("81234888452", telFind.get(0));
        assertEquals("81234888823", telFind.get(1));
        assertEquals("89564888823", telFind.get(2));

    }

    @Test
    void newString() {
        String test = findTel.newString();
        assertEquals(1000000, test.length());
    }
}