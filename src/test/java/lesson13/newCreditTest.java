package test.java.lesson13;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class newCreditTest {

    @Test
    void getCredit() {

        String credit = newCredit.getCredit("Bob", 30, 1000);
        assertEquals("отказано в кредите", credit);
        System.out.println(credit);
    }

    @Test
    void getCredit2() {

        String credit = newCredit.getCredit("Роман", 31, 3000);
        assertEquals("Кредит одобрен", credit);
        System.out.println(credit);
    }

    @Test
    void createJson() {
        newCredit.createJSON();
        assertNotEquals(0, newCredit.clients.length());

    }

    @Test
    void analizeJSON() {
        newCredit.createJSON();
        newCredit.analizeJSON();
        assertNotEquals(0, newCredit.clients.length());


    }


}



