package test.java.exam1;

import main.java.exam1.TestHelper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class TestHelperTest {

    @RepeatedTest(value = 10, name = "{displayName} №{currentRepetition}")
    void randomYearTest() {

        int y = TestHelper.randomYear();
        assert (y >= 1970 && y < 2021);

    }

    @RepeatedTest(value = 10, name = "{displayName} №{currentRepetition}")
    void longNumTest() {
        long l = TestHelper.longNum();
        assertNotEquals((Long) null, l);
    }

    @RepeatedTest(value = 10, name = "{displayName} №{currentRepetition}")
    void newStringTest() {
        String s = TestHelper.newString();
        assert (s.matches("[ ][A-z][a-z]*[ ][ A-z][a-z]*[ ][A-z][a-z]*[ ]"));

    }

    @Test()
    @DisplayName("newMapTest")
    void newMapTest() throws IOException {
        HashMap fruits = TestHelper.newMap("test/java/exam1/fileTest.txt");
        assertEquals("Апельсин", fruits.get(2));
        assertEquals("Арбуз", fruits.get(5));
    }

    @Test
    @DisplayName("newFormatTest")
    void newFormatTest() {

        Date d = new Date(1666666666666L);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-YYYY");
        String oldformat = dateFormat2.format(new Date(1666666666666L));
        System.out.println(oldformat);
        String s = TestHelper.newFormat(new Date(1666666666666L), "dd-MMMM-YYYY");
        assertEquals("25-октября-2022", s);
    }


    @ParameterizedTest
    @CsvSource({"29.08.1990, dd.MM.yyyy, dd-MMMM-yyyy, 29-августа-1990",
            "14-10-21, dd-MM-yy, dd-MMMM-yyyy E, 14-октября-2021 чт",
            "18-августа-2000, dd-MMMM-yyyy, dd.MM.yy, 18.08.00",
            "00:05 29 авг. 2021,HH:mm dd MMM yyyy, HH.mm.SS dd.MM.yy EEEE, 00.05.00 29.08.21 воскресенье"})
    void newFormatTest2(String date, String oldformat, String newformat, String assertDate) throws ParseException {
        String s = TestHelper.newFormat2(date, oldformat, newformat);
        assertEquals(assertDate, s);
    }

    @ParameterizedTest
    @CsvSource({"448645645,448645645.0", "-4486443242345645.5234532,-4486443242345645.5234532",
            "423423.4234324.343, Infinity", "одиндватри, Infinity"})
    void convertTest(String str, Double assertDouble) {
        Double d = TestHelper.convert(str);
        assertEquals(assertDouble, d);
    }

}