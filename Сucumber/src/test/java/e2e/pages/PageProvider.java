package e2e.pages;

import com.codeborne.selenide.Selenide;
import org.openqa.selenium.WebDriver;

import java.util.HashMap;

import static com.codeborne.selenide.WebDriverRunner.url;

public class PageProvider {
    public HashMap<String, BasePage> pages = new HashMap<>();


    public PageProvider() {

        pages.put("https://en.wikipedia.org/wiki/[a-zA-Z0-9]*$",
                  new ArticlePage());
        pages.put("https://www.wikipedia.org/",
                  new WikiIndexPage());
    }



    public BasePage find() throws Exception {

        for (String key : pages.keySet()) {
            if (url().matches(key)) {
                return pages.get(key);
            }
        }
        return new BasePage();
    }
}
