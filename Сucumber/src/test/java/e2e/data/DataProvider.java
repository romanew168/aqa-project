package e2e.data;

import java.util.HashMap;

public class DataProvider {
    private static HashMap<String, Object> data = new HashMap<>();
    static {
        data.put("password", "JHGHJsdfggGFC");
        data.put("good", "JHGHJGFC");
        data.put("Abrakadabra", "JHGHdfsgdJGFC");
        data.put("bad", "sdgfdsf");
        data.put("Kracks-Fex-Pax", "sdgsdfdsf");
    }

    public static void set(String key, Object value) throws Exception {
        if (data.containsKey(key)) {
            throw new Exception("Cannot set the key");
        }
        data.put(key, value);
    }

    public static Object get(String key) throws Exception {
        if (!data.containsKey(key)) {
            throw new Exception("No such key");
        }
        return data.get(key);
    }

    public static void update(String key, Object value) throws Exception {
        if (!data.containsKey(key)) {
            throw new Exception("No such key");
        }
        data.put(key, value);
    }
}
