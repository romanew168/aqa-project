package e2e.support;

import e2e.pages.PageProvider;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.screenshot;

public class CucumberHelper {

    @Before("not @no-browser")
    public void tearUp() {
        CucumberData.setDriver(new ChromeDriver());
        CucumberData.getDriver().manage().timeouts().pageLoadTimeout(Duration.ofSeconds(10));
        CucumberData.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @After("not @no-browser")
    public void tearDown() {
        CucumberData.getDriver().quit();
    }


    @AfterStep("not @no-browser")
    public void url() throws Exception {
        CucumberData.setProvider(new PageProvider());
        CucumberData.setPage(CucumberData.getProvider().find());
    }

    @AfterStep
    public void screen() {
        screenshot(System.currentTimeMillis() + ""); // save files to build/reports/tests/
    }
}
