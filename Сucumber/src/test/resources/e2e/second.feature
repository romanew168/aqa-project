@smoke
@krokodil
Feature: Wikipedia has Krokodil page

  Background: I am on the main page
    Given I am on the "https://wikipedia.org"

  Scenario: We can look for Krokodil page
    Given "searchQuery" is set "Krokodil"
    When I fill "searchField" with "searchQuery" value
    And I click the "searchButton"
    Then "mainHeader" text is "Krokodil"

  Scenario: We can look for Crocodile page
    Given "searchQuery2" is set "Crocodile"
    When I fill "searchField" with "searchQuery2" value
    And I click the "searchButton"
    Then "mainHeader" text is "Crocodile"

  Scenario: We can look for Крокодил page
    Given "searchQuery3" is set "Крокодил"
    When I fill "searchField" with "searchQuery3" value
    And I click the "searchButton"
    Then "mainHeader" text is "Крокодил"

  Scenario: We can look for Бульдог page
    Given "searchQuery4" is set "Бульдог"
    When I fill "searchField" with "searchQuery4" value
    And I click the "searchButton"
    Then "mainHeader" text is "Бульдог"

  Scenario: We can look for Гувер page
    Given "searchQuery5" is set "Гувер"
    When I fill "searchField" with "searchQuery5" value
    And I click the "searchButton"
    Then "mainHeader" text is "Гувер"