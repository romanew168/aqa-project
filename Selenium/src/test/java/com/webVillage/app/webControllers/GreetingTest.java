package com.webVillage.app.webControllers;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class GreetingTest {
    @LocalServerPort
    private Integer port;
    private String basePath;
    private static WebDriver driver;

    @BeforeEach
    public void getReady() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        driver = new ChromeDriver(options);
        basePath = "http://localhost:" + port;
        driver.get(basePath + "/greeting");
    }

    @ParameterizedTest
    @CsvSource(value = {
            "//h3;;Hello, World!",
            "//h3[2];;Find me!",
            "//h3[@class='header'];;And me!",
            "//h3[@class='header'][2];;NOW me!",
            "//h3[@class='header yellow'];;Do not forget about me",
            "//h3[@class='header'][@thestyle='red'];;It is simple",
            "//h3[contains(@class,'kitty-cat')][contains(@class,'abracadabra')];;My turn",
            "//h3[contains(@class,'bow-wow-dog')];;Where am I...",
            "//h3[contains(@class,'kitty-cat')][not(contains(@class,'abracadabra'))];;What about me",
            "//div[@id='good']/h3;;I am hiding",
            "//div[@id='bad']/h3;;U cannot get me!",
            "//div[@id='ugly']//h3;;Where is Blondie again?",
            "//p[@id='not_me']/..;;Find my daddy please.",
            "//p[@id='iAmLost']/ancestor::h3;;Look for my granny!",
            "//h3[@id='sister'];;Sister",
            "//h3[@id='sister']/../..//h3[not(@id='sister')];;Brother",
            "//div[@id='last']//h3[last()];;And me, please",
            "//div[@id='last']//h3[last()-1];;Find me!",
            "//h3[@price!=5];;Find me because my price is not 5",
            "//h3[@age>4];;Find me because my age is bigger than 4",
            "//h3[@courage>=5 and @courage<=15];;Find me because my courage is between 5 and 15",
            "//h3[text()='U can find me by text'];;U can find me by text",
            "//h3[contains(text(),'part')];;U can find me by the part of the text",
            "//h3[contains(text(),'U can find')][not(contains(text(), 'text'))];;U can find me by the absence of word t e x t",
            "//*[@*='pretender'];;Pretend that I'm constantly changing my tag. And find me"
    },
            delimiterString = ";;")
    public void testXpath(String xpath, String text) {
        WebElement found = driver.findElement(By.xpath(xpath));
        String txt = found.getText();
        assertEquals(text, txt);
    }

    @Test
    public void testXpathAdditional() {
        List<WebElement> elements = driver.findElements(By.xpath(
                "//h3[@class='two' or @class='three' or @class='four']"));
        assertEquals("Please", elements.get(0).getText());
        assertEquals("Find us", elements.get(1).getText());
        assertEquals("At once", elements.get(2).getText());
    }

    @ParameterizedTest
    @CsvSource(value = {
            "h3.header[thestyle*=e];;It is simple",   //атрибут thestyle содержит букву  e
            "h3.header[thestyle^=r];;It is simple",   //атрибут thestyle начинается на букву  r
            "h3.header[thestyle$=d];;It is simple",   //атрибут thestyle кончается на букву  d
    },
            delimiterString = ";;")
    public void testCss(String css, String text) {
        String txt = driver.findElement(By.cssSelector(css)).getText();
        assertEquals(text, txt);
    }


    @AfterEach
    public void endTest() {
        driver.quit();
    }
}
