package com.webVillage.app.webControllers;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class NewPersonControllerTest {
    @LocalServerPort
    private Integer port;

    private String basePath;

    private static WebDriver driver;

    @BeforeEach
    public void getReady() {
        ChromeOptions options = new ChromeOptions();
        //options.addArguments("--headless");
        driver = new ChromeDriver(options);
        basePath = "http://localhost:" + port;
        driver.get(basePath + "/person/new");
    }

    @Test
    public void test() throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Thread.sleep(1000);
        js.executeScript("document.querySelector('#firstName').value='Роман'");
        js.executeScript("document.querySelector('#secondName').value='Михайлов'");
        js.executeScript("document.querySelector('#age').value='31'");
        js.executeScript("document.querySelector('#occupation > option:nth-child(3)').selected=true");
        js.executeScript("document.querySelector('#gender > option:nth-child(1)').selected=true");
        Thread.sleep(2000);
        js.executeScript("document.querySelector('#doIt').click()");
        Thread.sleep(3000);
        String name = (String) js.executeScript("return document.querySelector('#name').innerText");
        assertEquals("NAME: Роман Михайлов", name);
        String occupation = (String) js.executeScript("return document.querySelector('#occupation').innerText");
        assertEquals("OCCUPATION: sheriff", occupation);
        String age = (String) js.executeScript("return document.querySelector('#age').innerText");
        assertEquals("AGE: 31", age);

    }

    @AfterEach
    public void endTest() {
        driver.quit();
    }

}
