package com.webVillage.app.webControllers;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class EditPersonControllerTest {

    @LocalServerPort
    private Integer port;

    private String basePath;

    private static WebDriver driver;

    @AfterEach
    public void endTest() {
        driver.quit();
    }

    @BeforeEach
    public void getReady() {
        ChromeOptions options = new ChromeOptions();
        //options.addArguments("--headless");
        driver = new ChromeDriver(options);
        basePath = "http://localhost:" + port;
        driver.get(basePath + "/person/new");
    }

    // Common problems
    // Element is under the other element
    // element click intercepted: Element <input type="text" id="firstName"> is not clickable at point (600, 128). Other element would receive the click: <input type="text" id="age">
    //
    // Element is hidden
    // element not interactable
    //
    // Cannot do action with element
    // element not interactable
    //
    // Element disappeared
    // stale element reference: element is not attached to the page document
    //
    // Element was not found
    // no such element: Unable to locate element: {"method":"css selector","selector":"input#abrakadabra"}

    @Test
    void getPerson() throws InterruptedException {
        WebElement name = driver.findElement(
                By.cssSelector("input#abrakadabra"));
        name.sendKeys("Hello");
        WebElement sure = driver.findElement(
                By.cssSelector("input#secondName"));
        sure.sendKeys("Hello");
        WebElement age = driver.findElement(
                By.cssSelector("input#age"));
        age.sendKeys("55");
        driver.findElement(
                By.cssSelector("#doIt")).click();
        Thread.sleep(30000);
        age.sendKeys("44");

//        input.sendKeys(Keys.ZENKAKU_HANKAKU,
//                Keys.SEMICOLON, "hhhh", Keys.BACK_SPACE);
//        Thread.sleep(3000);
//        input.click();
//        input.sendKeys(String.valueOf(input.isDisplayed()));
//        input.sendKeys(String.valueOf(input.isEnabled()));
//        input.sendKeys(String.valueOf(input.isSelected()));
//        Thread.sleep(3000);
    }

    @Test
    void iframe() throws InterruptedException {
        driver.switchTo().frame("lol");
        String lol = driver.findElement(By.cssSelector("h1.text-center")).getText();
        driver.switchTo().parentFrame();
        WebElement input = driver.findElement(
                By.cssSelector("input#firstName"));
        input.sendKeys(lol);
        Thread.sleep(3000);
    }
}